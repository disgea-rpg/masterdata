masterdata of the mobile gacha game Disgea RPG.

* encrypted - original files
* parsed    - converted to json
* translation - jap. string converted to englisch via Google Translate
* patch     - translation patch files

### decryption

1. The original files are encrypted via AES with zeroes padding (\x00 's at the end).
* AesInitVector   : aO5yK7tOlu1XkKTS
* AesKey          : Ze3DZ8mKkovba/mYz7Z/AUkguQfEvnnM

2. The data in those files is packed via [msgpack](https://msgpack.org/index.html)

### example code
* Python
```python
import msgpack
from Crypto.Cipher import AES
AesBlockSize = 128
AesKeySize = 256
AesIVSize = 16
AesInitVector = b'aO5yK7tOlu1XkKTS'
AesKey = b'Ze3DZ8mKkovba/mYz7Z/AUkguQfEvnnM'.ljust(16, b"\0")


def Encrypt(data):
	#	zeroes padding
	data+=b'\x00'*(16-len(data)%16)
	cipher = AES.new(AesKey, AES.MODE_CBC, AesInitVector)
	return cipher.encrypt(data)

def Decrypt(data):
	cipher = AES.new(AesKey, AES.MODE_CBC, AesInitVector)
	return cipher.decrypt(data).rstrip(b'\x00')

def Unpack(data):
	return msgpack.unpackb(data,raw=False)

def Pack(data):
	return msgpack.packb(data)
```